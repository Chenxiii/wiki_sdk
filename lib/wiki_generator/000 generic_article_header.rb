class WikiGenerator
  # Generic header for article.
  #
  # This shows a navigator allowing the user to :
  # - Go back
  # - Read the next article
  # - Go to an article of the section by selecting it in the dropdown
  class GenericArticleHeader
    # GOTO in each lang
    GOTO_TRANSLATION = {
      'fr' => 'Aller à :',
      'en' => 'Goto :',
      'de' => 'gehe zu:',
      'it' => 'vai a:',
      'es' => 'ir a:',
      'ja' => 'Goto:',
      'ko' => 'Goto:'
    }
    # Create a new GenericArticleHeader
    # @param titles [Array<String>] title of each article
    # @param dir [String] folder containing the article
    # @param articles [Array<String>] list of articles filename
    # @param langs [Array<String>] list of langs for the section
    # @param lang [String] current lang
    def initialize(titles, dir, articles, langs, lang)
      @titles = titles
      @articles = articles
      @dir = dir
      @langs = langs
      @lang = lang
    end

    # Process the header for a specific page
    # @param index [Integer]
    # @return [String] the article header
    def process(index)
      @index = index
      wrap(generate_previous, generate_article_selector, generate_next, generate_lang_selector)
    end

    private

    # Wrap each part of the article header
    # @param previous [String] previous article
    # @param article_selector [String] article selector
    # @param next_art [String] next article
    # @param lang_selector [String] language selector
    # @return [String]
    def wrap(previous, article_selector, next_art, lang_selector)
      <<~WRAPPER
        <nav class="article_header_nav">
          #{previous} #{article_selector} #{next_art}
          #{lang_selector}
        </nav>
      WRAPPER
    end

    # Generate the language selector
    # @return [String]
    def generate_lang_selector
      return "<div class=\"language_selection\">💬 : #{LANG_TRANSLATIONS[@lang]}</div>" if @langs.size <= 1

      langs = @langs.reject { |lang| lang == @lang }
      article = @articles[@index]
      lang_links = langs.collect do |lang|
        "<a href=\"#{lang}/#{@dir}/#{article}.html\">#{LANG_TRANSLATIONS[lang]}</a>"
      end

      return <<~SELECTOR
        <div class="language_selection"><details class="dropdown">
          <summary>💬 : #{LANG_TRANSLATIONS[@lang]}</summary>
          <div>#{lang_links.join}</div>
        </details></div>
      SELECTOR
    end

    # Generate the article selector
    # @return [String]
    def generate_article_selector
      return '<div class="article_selection"></div>' if (@index == 1 && @articles.size < 4) || @articles.size < 3
      dir = "#{@lang}/#{@dir}"
      article_links = @articles.collect.with_index do |article, i|
        next if i == @index
        "<a href=\"#{dir}/#{article}.html\">#{@titles[i]}</a>"
      end
      return <<~ARTICLE_SELECTION
        <div class="article_selection">
          <span>#{GOTO_TRANSLATION[@lang]}</span>
          <details class="dropdown">
            <summary>#{@titles[@index]}</summary>
            <div>#{article_links.join}</div>
          </details>
        </div>
      ARTICLE_SELECTION
    end

    # Generate the previous article link
    # @return [String]
    def generate_previous
      return '<div class="previous_article"></div>' if @index == 0

      index = @index.pred
      return <<~PREVPART
        <div class="previous_article">
          <a href="#{@lang}/#{@dir}/#{@articles[index]}.html">« #{@titles[index]}</a>
        </div>
      PREVPART
    end

    # Generate the next article link
    # @return [String]
    def generate_next
      return '<div class="next_article"></div>' if @index >= (@articles.size - 1)

      index = @index.next
      return <<~NEXTPART
        <div class="next_article">
          <a href="#{@lang}/#{@dir}/#{@articles[index]}.html">#{@titles[index]} »</a>
        </div>
      NEXTPART
    end
  end
end

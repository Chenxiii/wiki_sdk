# Installer un script

PSDK possède plusieurs manières pour installer un script. Vous pouvez utiliser :
- L'Éditeur de Scripts RMXP (copier le script au-dessus de Main)
- Le dossier `scripts`
- `Git`

![tip](danger "Il y a un dossier pokemonsdk/scripts. Vous ne devez jamais modifier ces scripts sous peine de perdre vos modifications lors des mises-à-jour de PSDK.")

## Le dossier Scripts

A la racine de votre projet, il y a un dossier `scripts`. C'est là que vous pourrez télécharger vos scripts ou les créer. 

Dans ce dossier, tous les noms de dossiers ou de scripts doivent commencer par 5 chiffres. 

Par exemple :
- Un simple script devrait s'appeler : `scripts/00523 My Script.rb`.
- Un script à l'intérieur d'un dossier devrait se nommer : `scripts/01236 Folder Name/03129 My Script.rb`

L'ordre de chargement des scripts dépend des chiffres donnés aux scripts. Les dossiers sont toujours chargés après les scripts de même niveau.

## Utiliser Git
### Si votre projet n'est pas une repository git

Si vous n'utilisez pas git pour votre projet, vous devriez utiliser la commande `git clone`.

Par exemple, si vous voulez installer le [DynamicLightScript](https://gitlab.com/NuriYuri/dynamiclightscript) vous devez suivre les instructions suivantes :
1. Ouvrez `cmd.bat`
2. Exécutez la commande `cd scripts`
3. Exécutez la commande `git clone https://gitlab.com/NuriYuri/dynamiclightscript.git "00500 NuriYuri DynamicLight"`  
    ![tip](info "Nous avons définit un nom dans la commande, vous devriez donc voir les scripts dans `00500 NuriYuri DynamicLight`")
    ![tip](warning "Parfois il y a des ressources à ajouter à votre projet, lisez le README.md car pour cet exemple, il y a des ressources graphiques à télécharger et à installer!")

### Si votre projet est une repository git

Si vous utilisez git pour votre projet, les commandes seront différentes. Nous utiliserons `git submodule add`. De cette façon, le projet se réfèrera au script plutôt que de le copier et d'être incapable de le mettre à jour ensuite.

Par exemple, si vous voulez installer le [DynamicLightScript](https://gitlab.com/NuriYuri/dynamiclightscript) vous devez suivre les instructions suivantes :
1. Ouvrez `cmd.bat`
2. Exécutez la commande `cd scripts`
3. Exécutez la commande `git submodule add https://gitlab.com/NuriYuri/dynamiclightscript.git "00500 NuriYuri DynamicLight"`  
    ![tip](info "Nous avons définit un nom dans la commande, vous devriez donc voir les scripts dans `00500 NuriYuri DynamicLight`")
    ![tip](warning "Parfois il y a des ressources à ajouter à votre projet, lisez le README.md car pour cet exemple, il y a des ressources graphiques à télécharger et à installer!")

### Mettre à jour un script depuis git

Comme vous utilisez git, il est plus simple de mettre à jour les scripts, vous avez simplement à vous déplacer dans le dossier du script et à écrire `git pull`.

Exemple avec le script DynamicLight :
1. Ouvrez `cmd.bat`
2. Exécutez la commande : `cd "scripts/00500 NuriYuri DynamicLight"`
3. Exécutez : `git pull`

Maintenant vos scripts sont à jour!
# Corriger les problèmes

Cette page liste des problèmes connus que vous pouvez rencontrer sous PSDK

## Les RubyFPS sont bas

Généralement les RubyFPS doivent toujours être plus grand que 200. Si jamais c'est pas le cas et que votre machine est correcte, posez vous la question suivante : Est-ce que ma carte graphique ~~coute beaucoup trop cher~~ est une Nvidia ?  
Si la réponse est oui, alors la solution se trouve dans l'image suivante:

![Nvidia Control Panel|Center](img/nvidia.png "Désactivez l'optimisation threadé")

## La jeu ne démarre pas

Il arrive que le je ne veuille pas démarrer, dans un tel cas, nous ne pouvons rien faire **à moins que vous suiviez la procédure suivante** :

1. Double cliquez sur `cmd.bat` à la racine de votre projet (`cmd` si les extensions sont cachées).
2. Ecrivez `game debug` et appuyez sur entrée.
3. Attendez jusqu'à ce que vous puissiez entrer de nouvelles commandes.
4. Cliquez dans la partie sombre de la console, appuyez sur `CTRL+A` deux fois puis sur `CTRL+C` (ça devrait copier le contenu intégral de la console).
5. Ouvrez le bloc note (`Window+R` => `notepad`)
6. Collez le contenu de la console à l'aide de `CTRL+V`
7. Sauvegardez le fichier et faites un rapport de bug sur le forum en liant le contenu du fichier à votre message.

Nous vous dirrons ce qui ne va pas.

# Pokémon editor

![tip](warning "This page comes from the **old Wiki**. It's probably not up to date.")

## Pokémon editor UI

The Pokémon Editor UI is accessible from the `Pokémon` button in the Home.

![Pokémon UI|center](img/ruby_host/Rubyhost_pokemonmain.png "Main UI of the Pokémon editor")

## Create a new Pokémon

1. Make sure [the text of the Pokémon are added](text.md) (Name + Description + Specie)
2. Click on `Add Pokémon`.
4. Set all its informations
    ![tip](info "The `check evolution` button helps you to make sure you properly set the Pokémon evolution up.")
5. Click on `Moveset editor`. A new Window opens to let you edit the Pokémon move set.
    - Select `New`, choose the `Move in database` and the `level`.
    - Don't forget to `Save`.  
        ![Editeur de Moveset|center](img/ruby_host/Rubyhost_pokemonmovepool.png "Moveset editor")
6. Click on `Tech, breedmove & item editor`. A new window opens and allow you to choose the tech set of the Pokémon.
        ![Editeur de CT / breedmoves & objets|center](img/ruby_host/Rubyhost_pokemonctbreeditem.png "Éditeur de CT / breedmove et objet porté")
7. Once the modifications are done, don't forget to click on `Save`

## Set the Pokémon Specific evolution

Here's the list of `Special evolution : (Array of Hash)` keys you can use :

- Level (`:min_level` et `:max_level`).
- Trade (`:trade_with`, `:trade`) : ID of the Pokémon.
- Using a stone (`:stone`) : ID of the stone (item).
- Holding an item (`:item_hold`) : ID of the item.
- Happyness (`:min_loyalty` et `:max_loyalty`).
- By move learnt (`:skill_X` with `X` from 1 to 4) : ID of the move.
- Weather (`:weather`) : ID of the weather.
- On a SystemTag (`:env`) : Type of tag.
- By gender (`:gender`) : 0 = none, 1 = male, 2 = female.
- According to the current day cycle (`:day_night`).
- Using a ruby function (`:func`) : Symbol of the function to call to make sure the evolution is ok.
- Using the current Map ID (`:maps`) : Array of Integer.

Here's some example according to the official :

### Bulbasaur evolution

In `Natural (ID):`, enter 2 (Ivysaur ID) and in `Level:`, enter 16.

### Trade evolution of Machoke

Leave `Natural (ID):` and `Level:` empty.  
In `Special evolution : (Array of Hash)` enter :
```ruby
[ { :trade => 68 } ]
```
![tip](info "68 is Machamp's ID")

### Weepinbell's stone evolution

Leave `Natural (ID):` and `Level:` empty.  
In `Special evolution : (Array of Hash)` enter :
```ruby
[ { :id => 71, :stone => 85 }]
```
![tip](info "71 is Victreebel's ID and 85 is the Plant Stone ID")

### Onix evolution (holding an item)

Leave `Natural (ID):` and `Level:` empty.  
In `Special evolution : (Array of Hash)` enter :
```ruby
[ { :trade => 208, :item_hold => 233 } ]
```
![tip](info "208 is Steelix's ID and 233 is the Metal Coat ID")

### Cleffa evolution (happyness)

Leave `Natural (ID):` and `Level:` empty.  
In `Special evolution : (Array of Hash)` enter :
```ruby
[{:id=>35, :min_loyalty=>220}]
```

![tip](info "35 is Clefairy's ID and 220 is the minimum happyness")

### Piloswine's evolution (Move learnt)

Leave `Natural (ID):` and `Level:` empty.  
In `Special evolution : (Array of Hash)` enter :
```ruby
[ { :id => 473, :skill_1 => 246 } ]
```
![tip](info "473 is Mamoswine's ID and 246 is Ancient Power's ID")

### Combee's evolution (gender)

Leave `Natural (ID):` and `Level:` empty.  
In `Special evolution : (Array of Hash)` enter :
```ruby
[ { :gender => 2, :min_level => 21, :id => 416 } ]
```
![tip](info "2 is female, 21 the minimum level and 416 Vespiquen's ID")

### Evolution to Mentali (day night cycle & happyness)

Leave `Natural (ID):` and `Level:` empty.  
In `Special evolution : (Array of Hash)` enter :
```ruby
[ { :id => 196, :min_loyalty => 220, :day_night => 3 } ]
```
![tip](info "196 is Mentali's ID, 220 is the minimum happyness, 3 correspond to `Day` (according to `::Yuki::Var::TJN_Tone`")


### Babimanta's evolution (function)

Leave `Natural (ID):` and `Level:` empty.  
In `Special evolution : (Array of Hash)` enter :
```ruby
[ { :id => 226, :func => :elv_demanta } ]
```
![tip](info "226 is Mantine's ID and `:elv_demanta` is the scripted function in `PFM::Pokemon` that checks if Remoraid is in the team.")

### Magnéton's evolution (according to a location)

Leave `Natural (ID):` and `Level:` empty.  
In `Special evolution : (Array of Hash)` enter :
```ruby
[ { :id => 462, :maps => [X] } ]
```
![tip](info "462 is Magnezone's ID eandt `X` is the RMXP map ID (5 for 005)")

## MEGA-parameters

The MEGA evolution rely on the Special Evolution field but the settings are done in the MEGA form of the Pokémon.

1. Choose the Pokémon you want to set its Mega form up `Pokémon :`.
2. Choose the `Current form :` 30 or 31 (that's MEGA-1 & MEGA-2)
3. In `Special evolution : (Array of Hash)`, enter `[{:gemme=>X}]` (where X is the ID of the MEGA-Gem in the item database).

Don't forget to `Save`.
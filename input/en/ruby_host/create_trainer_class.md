# Create a Trainer class

You might have already read the [PSDK Trainer tutorial](trainer.html) and you would like to change the class of your trainer or you got this error, 

![TrainerError](img/ruby_host/en/trainer_error.png)

In that case, follow this tutorial.

## Add a Trainer class for a new trainer (to solve the aforementioned error)

Open RubyHost in the Trainers tab:

![TextsTab](img/ruby_host/en/trainer.png)

Click on ``Edit trainer class``

Then, click on ``Add text`` and fill the fields according to your game languages: 

![Fill](img/ruby_host/en/fill.png)

## Modify a Trainer Class of an existing Trainer

Select the Trainer ID (here it's 127):

![ID127](img/ruby_host/en/id127.png)

Replace the names in the fields according to your game languages: 

![Blankfields](img/ruby_host/en/fields.png)

![tip](warning "Don't forget to ``Validate`` and ``save`` for each modification in order to apply it.")
# Zone editor

![tip](warning "This page comes from the **old Wiki**. It's probably not up to date.")

## Zone UI
By clicking on the `Zones` button from the Home UI you can access to the editor
![Editeur Zone|center](img/ruby_host/Rubyhostmapsmain.png "Zone editor")

It allows to :
- Create a zone and set the panel to show when entering the map.
- Allow or Forbid fly/teleport/dig.
- Set the player position on worldmap when he opens the Map.
- Set the warp location.
- Set the default weather.

## Create a new Zone

1. Click on `+`
    ![Illustration](img/ruby_host/Plus.png)
2. Enter the MAP ID or the list of MAP ID that are in the zone in `Map ID` field.
3. Enter then panel id to show in `panel id shown` field (0 = no panel).
4. Check the right teleportation flags.
5. Enter the teleportation position (when player fly to the map) `X :` and `Y :` in fly data. (Leave blank to forbid flying to this zone).
6. Enter the Player's coordinate on the worldmap.
7. Click on `Save`.
8. To change the zone name, go to the text editor in file 10 :  
    ![Edition de nom de zone](img/ruby_host/Test.png)

## Make a wild groupd for this zone
Go back to the Home UI and click on `Wild groups`. This will open the Wild Group editor :
![Editeur de Groupe|center](img/ruby_host/Rubyhost_mapswildeditor.png "Wild Group editor")

To create a new group select the right zone and :
1. Click `New group` in `Groups`.
2. Set the `Group Info`  
    ![tip](info "The level noise correspond to the span of level around the regular level of the Pokémon. Example: if the Pokemon is level 5 and the group level noise is 3, you can encounter level 4, 5 and 6 of this Pokémon.")
3. Choose the Pokémon you want to add (under `Pokémon`) and click on `Add`.
4. Select a Pokémon in the list of `Pokémon`.
5. Set its information (form etc...).
6. Save

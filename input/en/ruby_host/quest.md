# Quests editor

![tip](warning "This is for **creating** quests, to learn how to manage them go [here](https://psdk.pokemonworkshop.fr/wiki/en/event_making/quest.html). This editor is not really practical and will probably change in the future.")

## The interface

This is what the Quest editor looks like:
![Interface|center](img/ruby_host/quest_en.png "Quest editor")

It contains everything you need to set the quest objective & rewards.

### Choice of an new objective

In the `Objective type:` dropdown you have the following choices:
- Speak to an NPC : fill the `Name:` field
- Beat an NPC : fill the `Name:` field
- Find an Item : choose the item in `Item:` and fill the `Amount:` field
- Find a Pokémon : fill the `Amount:` field and choose the Pokémon in `Pokémon:`
- Beat a Pokémon : same
- Capture a Pokémon : same
- Obtain an Egg : fill the `Amount:` field
- Hatch an Egg : fill the `Amount:` field

Once the objective is configured, click on `Add` under the list. (You can't choose the order.)

### Remove an objective

Choose the objective you want to remove in the list of objectives an click on the `Remove` button under the list.

### Earning choice

The `Reward type` dropdown allows you to choose between two kinds of rewards:
- `Money` : fill the `Amount:` field
- `Item` : choose the item in `Item:` and fill the `Amount:` field

You can choose multiple rewards, and the removal works the same as with the objectives but is under the reward list instead.